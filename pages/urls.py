from django.urls import path

from . import views

urlpatterns = [
    path('home/', views.ShowHome, name='home'),
    path('profile/', views.ShowProfile, name='profile'),
    path('key/', views.ShowKey, name='key'),
    path('this_week/', views.ShowThisWeek, name='this_week'),
    path('today/', views.ShowToday, name='today'),
]
