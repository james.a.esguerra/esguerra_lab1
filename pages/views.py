import datetime
from django.shortcuts import render
from django.http import HttpResponse
from .forms import HomeForm


def ShowHome(request):
    if request.method == 'POST':
        form = HomeForm(request.POST)
        if form.is_valid():
            global name
            name = form.cleaned_data['name']
            #renders another HTML with the user's name and without the form
            return render(request, 'home2.html', {'name': name})

    form = HomeForm()
    return render(request, 'home.html', {'form': form})


def ShowProfile(request):
    return render(request, 'profile.html', {'name': name})


def ShowKey(request):
    return render(request, 'key.html')


def ShowThisWeek(request):
    return render(request, 'this_week.html')


def ShowToday(request):
    my_date = datetime.datetime.now()
    return render(request, 'today.html', {'my_date': my_date})
